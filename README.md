# Weather App

Weather app is built in Angular where you can get weather data.

## OpenWeatherMap API

OpenWeatherMap API is used to fetch data in this app.

`https://openweathermap.org`

## Live Demo

[Check here](https://alpertoy.gitlab.io/weather-app/)
