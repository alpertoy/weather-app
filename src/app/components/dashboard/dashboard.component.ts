import { WeatherService } from './../../services/weather.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  image = 'assets/images/weather.png';
  city = '';
  temperature = 0;
  humidity = 0;
  weather = '';
  query = false;
  loading = false;
  showError = false;

  constructor(private weatherService: WeatherService) { }

  ngOnInit(): void {
  }

  getWeather() {
    this.query = false;
    this.loading = true;

    this.weatherService.getWeather(this.city).subscribe(data => {
      this.loading = false;
      this.query = true;
      this.temperature = data.main.temp -273;
      this.humidity = data.main.humidity;
      this.weather = data.weather[0].main;
    }, error => {
      this.loading = false;
      this.error();
    })
  }

  error() {
    this.showError = true;
    setTimeout(() => {
      this.showError = false;
      this.city = '';
    }, 6000);
  }

}
